// Importar el modelo de la base de datos para el CRUD
const modeloCita = require('../models/modelCita')


// Exportar en diferentes variables los métodos para el CRUD

//CRUD => Create     -------------------------------------------------------------------------------
exports.crear = async (req, res) => {

    try {

        let cita;

        cita = new modeloCita({
            id: 13,
            nonbremedico: "Tata",
            cita: "Laboratorio",
            paciente: 13,
            fechahora: 212202110
        });

        await cita.save();
        res.send(cita);


    }catch (error){
        console.log(error);
        res.status(500).send('Error al guardar el la cita')
    }
}


//CRUD => Read  ---------------------------------------------------------------------------------
exports.obtener = async(req, res) => {

    try {

        const cita =  await modeloCita.find();
        res.json(cita);       

    } catch (error){
        console.log(error);
        res.status(500).send('Error al guardar la / o las citas')
    }
}

//CRUD   => Update   -----------------------------------------------------------------------------
exports.actualizar = async(req, res) => {

    try {

        const cita =  await modeloCita.findById(req.params.id);
        if (!cita){
            console.log(cita);
            res.status(404).json({msg: 'La cita no existe'})
        }
        
        else {
            await modeloCita.findByIdAndUpdate({_id: req.params.id}, {cita: medicinag});
            res.json({msg: 'Cita actualizada corectamente'});
        }
    } catch (error){
        console.log(error);
        res.status(500).send('Error al editar la cita en update');
    }
}


//CRUD   =>  Delete   --------------------------------------------------------------------------------
exports.eliminar = async(req, res) => {

    try {

        const cita =  await modelCita.findById(req.params.id);
        if (!cita){
            console.log(cita);
            res.status(404).json({msg: 'La cita no existe'});
        } 

        else {
            await modeloCita.findByIdAndRemove({_id: req.params.id});
            res.json({msg: 'Cita eliminada corectamente'});
        }

    } catch (error){
        console.log(error);
        res.status(500).send("Error al eliminar la / o las citas")
    }
}




