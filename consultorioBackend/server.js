// Forma convencional de ES6 => Esandar conventional de js  --------javascrip
//import  express  from "express";
// Forma sistema nativo de NOdeJS
//const { Router } = require('express');

const express = require('express');

const router = express.Router();

//Importar módulo de MongoDB  => mongoose
// const mongoose = require('mongoose');

// Importación de la variable de entorno
//require('dotenv').config({path: 'var.env'})

//Importar modulo a la base de datos  ---- es decir archivo db.js
//---- De esta forma se decentraliza un punto de conexión para dar mayor seguridad a al aplicación----
const conectarDB = require('./config/db');


let app = express(); 

app.use('/', function(req, res){
    res.send("---------------------- Hola compañeros  lista la aplicación ------------------");
});

app.use(router);

//conexión con la base de datos --- usando variables de entorno  desde el arcrivo descentralizado db ---  
conectarDB();

//console.log(process.env.URL_MONGODB);
//mongoose.connect(process.env.URL_MONGODB)
//    .then(function(){console.log("Conexión lista con MongoDB Atlas")})
//    .catch(function(e){console.log(e)})


//Modelo esquema Shema de la base de datos => es 
// const citaSchema = new mongoose.Schema({
//     id: Number,
//     nonbremedico: String,
//     cita: String,
//     paciente: Number,
//     fechahora: Number 
// });

// Modelo de la cita => 
// Se debe terner en cuenta el esquema y las colecciones planteadas
// const modelocita = mongoose.model('cita_medica', citaSchema);


//CRUD   => create -----------------------------------------------------------------------------------
// modelocita.create(
//     {
    
//         id: 11,
//         nonbremedico: "Eduardo",
//         cita: "Laboratorio",
//         paciente: 11,
//         fechahora: 212202110
//     },
//     (error) => {
//         console.log("Ingreso función error...")
//         if (error) return console.log(error);
//         console.log(error);
//         console.log("Sale de la función error...")
//     }
// );


//CRUD   => Read   para leer y traer de la base de datos  --------------------------------------------
// modelocita.find((error, cita) => {
//     if (error) return console.log(error);
//     console.log(cita)
// });


//CRUD   => Update   -----------------------------------------------------------------------------
// modelocita.updateOne({id: 12}, {id: 12, nonbremedico: "Ángel", cita: "Laboratorio", paciente: 12, fechahora: 3011202111}, (error) => {
//     if (error) return console.log(error);
// });


//CRUD   =>  Delete   --------------------------------------------------------------------------------
// modelocita.deleteOne({id: 12}, (error) => {
//     if (error) return console.log(error);
// });



//----------------------------------------------------------------------------------------------------
//######################         Descentralizaci+on del CRUD            #############################
//                                Rutas desde controlCitas
// Uso de archivos tipo json en la variable app
app.use(express.json());

// CROOS =>  Mecanismos de seguridad para el control de peticiones http   Get- post -put

const cors = require('cors');

// Solicitudes al CRUD   =>  el controlador
const crudCitas = require('./controllers/contolCita');

// Establecer rutas respecto de CRUD

// CRUD => Crear
router.post('/', crudCitas.crear);

// CRUD => Read
router.get('/', crudCitas.obtener);

// CRUD => Update
router.put('/', crudCitas.actualizar);

// CRUD => Delete
router.delete('/', crudCitas.eliminar);

//----------------------------------------------------------------------------------------------------

//Uso de funciones flecha
router.get('/metodoget', (req, res) => {
    res.send("Uso del método GET...")
//    conectarDB();
// Conexión con metodo usando metodo descentralizado db.js esta es opcional    
    
});
//----------------------------------------------------------------


//router.get('/metodoget', function(req, res){
//    res.send("Uso del método GET...")
//});

router.post('/metodoget', function(req, res){
    res.send("Uso del método POST... desde la ruta / métodoget")
});

router.post('/metodopost', function(req, res){
    res.send("Ruta / métodopost... usando méto POST")
});

//Conexión con la base de datos
//const user = "admin";
//const psw = "ayrJgtH*324";
//const db = "consultorio";
//const url = `mongodb+srv://${user}:${psw}@consultorio.5e8qy.mongodb.net/${db}?retryWrites=true&w=majority`;

//mongoose.connect(url)
//    .then(function(){console.log("Conexión lista con MongoDB Atlas")})
//    .catch(function(e){console.log(e)})


app.listen(4000);

console.log("la aplicación ya funciona bien y se ejecuta en http://localhost:4000");


