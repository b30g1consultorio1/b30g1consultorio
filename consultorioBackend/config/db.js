//Importar módulo de MongoDB  => mongoose
const mongoose = require('mongoose');

// Importación de la variable de entorno
require('dotenv').config({path: 'var.env'});

const conexionDB = async () => {
    try {
        await mongoose.connect(process.env.URL_MONGODB, {});
        
        console.log("Lista la conexión con MongoDB Atlas desde el archivo db.js");

    } catch (error) {
        
        console.log("Error de conexión con base de datos.");
        console.log(error);
        process.exit(1);
    }
}
// exportat como módulo para utilizar en otros varios arcivos

module.exports = conexionDB;


