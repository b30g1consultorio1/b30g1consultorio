// Este apartado contiene la estructura básica de la asignación de las citas médicas

//Importar módulo de MongoDB  => mongoose
const mongoose = require('mongoose');

const citaSchema = mongoose.Schema({
    id: Number,
    nonbremedico: String,
    cita: String,
    paciente: Number,
    fechahora: Number 
});

module.exports = mongoose.model('cita_medica', citaSchema);
