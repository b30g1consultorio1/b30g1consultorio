Parte 1

Instrucciones de la línea de comandos
También puede cargar archivos existentes desde su computadora siguiendo las instrucciones a continuación.


Configuración global de Git
git config - nombre de usuario global "Eduardo Gutiérrez"
git config --global user.email "universidadmatematica@gmail.com"

Crea un nuevo repositorio
clon de git https://gitlab.com/b30g1consultorio1/b30g1consultorio.git
cd b30g1consultorio
git switch -c principal
toque README.md
git agregar README.md
git commit -m "agregar README"
git push -u origen principal

Empuje una carpeta existente
cd carpeta_existente
git init --initial-branch = main
git remoto agregar origen https://gitlab.com/b30g1consultorio1/b30g1consultorio.git
git add.
git commit -m "Confirmación inicial"
git push -u origen principal

Empuje un repositorio de Git existente
cd existente_repo
git remoto renombrar origen origen antiguo
git remoto agregar origen https://gitlab.com/b30g1consultorio1/b30g1consultorio.git 
git push -u origen --todos
git push -u origen --tags
